---
title: "Les fonctions Ninjas de Kotlin [AK 2]"
categories: fr coding tutoriel android kotlin
author: macha
---

<div class="text-center lead" markdown="1">
  ![Android Kotlin](/assets/img/post/android-kotlin.png)
</div>

Cet article présente les fonctions standard suivantes, inhérentes aux langage _Kotlin_ :

 + `with`
 + `run`
 + `let`
 + `also`
 + `apply`

Surnommées les «fonctions Ninjas» par Antonio Leiva[^6], ces fonctions font toutes
la même chose a quelques détails prêts.[^8] De manière général,
elles exécutent toutes un ensemble d'instructions sur une instance.
<!--more-->

## Fonction avec paramètre explicite `with`
La fonction `with` prend un paramètre et exécute un bloc d'instructions sur
ce dernier. Elle retourne le résultat du bloc d'instructions.

Par exemple :
```kotlin

```
L'intérêt est d'avoir un code propre et plus lisible, de façon à éviter les erreurs
d'implémentation. Par conséquent, utiliser des fonctions Ninjas, c'est assurer
la maintenabilité d'un projet d'envergure.

La fonction `with`[^1] a pour signature :
```kotlin
inline fun <T, R> with(receiver: T, block: T.() -> R): R
```
Remarquez le type retour.

## Fonction d'extension ou fonction simple `run`
La fonction d'extension `run` exécute un bloc d'instructions sur l'instance du type étendu (le receveur), `this` est alors envoyé comme argument. Elle retourne le résultat du bloc d'instructions.

Les fonctions `with` et `run` sont presque identiques, la nuance est sur l'utilisation avec un objet possiblement nulle.
Par exemple avec `with` :
```kotlin
with(webview.settings) {
      this?.javaScriptEnabled = true
      this?.databaseEnabled = true
   }
}
```
En revanche avec `run`, le bloc d'instruction n'est pas exécuté si `var` est nulle :
```kotlin
webview.settings?.run {
    javaScriptEnabled = true
    databaseEnabled = true
}
```

La fonction `run`[^2] a pour signature :
```kotlin
inline fun <T, R> T.run(block: T.() -> R): R
```
ou
```kotlin
inline fun <R> run(block: () -> R): R
```

## Fonction lambda `let`
La fonction lambda `let` exécute un bloc d'instructions, `it` est envoyé comme argument.
Elle retourne le résultat du bloc.

Les fonctions `run` et `let` sont presque pareilles,
/**la subtilité
réside sur l'instance sur lequel le bloc d'instructions est exécuté. Avec `let`,
il faut expliciter sur quoi les instructions sont exécutés**/

Avec `let` les instructions s'applique sur `it`,
ce dernier doit être explicité :
```kotlin
stringVariable?.let {
      println("The length of this String is ${it.length}")
}
```
En revanche avec `run` ou `with`, les instructions s'applique sur `this`,
ce dernier peut être omis :
```kotlin
stringVariable?.run {
      println("The length of this String is ${length}")
}
```

La fonction `let`[^3] a pour signature :
```kotlin
inline fun <T, R> T.let(block: (T) -> R): R
```
Pour résumer :

 | this | it
:- |:-: | -:
Retourne le résultat | `run` `with` | `let`

## Fonction lambda `also`
La fonction `also`, Elle retourne `it`.

Les fonctions `let` et `also` sont presque pareille dans le sens où les instructions s'applique sur `it`. La subtilité réside dans le fait que `also` retourne `it`.
Par exemple :
```kotlin
// Evolve the value and send to the next chain
original.let {
    println("The original String is $it") // "abc"
    it.reversed() // evolve it as parameter to send to next let
}.let {
    println("The reverse String is $it") // "cba"
    it.length  // can be evolve to other type
}.let {
    println("The length of the String is $it") // 3
}
// Wrong
// Same value is sent in the chain (printed answer is wrong)
original.also {
    println("The original String is $it") // "abc"
    it.reversed() // even if we evolve it, it is useless
}.also {
    println("The reverse String is ${it}") // "abc"
    it.length  // even if we evolve it, it is useless
}.also {
    println("The length of the String is ${it}") // "abc"
}
```

L'intérêt est de pouvoir re-écrire certaine fonction comme `makeDir()`:
```kotlin
// Normal approach
fun makeDir(path: String): File  {
    val result = File(path)
    result.mkdirs()
    return result
}
```
Avec `let` et `also`, `makeDir()` devient une simple expression :
```kotlin
// Improved approach
fun makeDir(path: String) = path.let{ File(it) }.also{ it.mkdirs() }
```

La fonction `also`[^4] a pour signature :
```kotlin
inline fun <T> T.also(block: (T) -> Unit): T
```

## Fonction d'extension `apply`
La fonction d'extension `apply` exécute un bloc d'instruction sur l'instance du type étendu (le receveur), `this` est envoyé comme argument. Elle retourne le type étendu, c'est-à-dire le receveur lui-même, `this`.

Par exemple :
```kotlin
// Normal approach
fun createInstance(args: Bundle) : MyFragment {
    val fragment = MyFragment()
    fragment.arguments = args
    return fragment
}
// Improved approach
fun createInstance(args: Bundle)
              = MyFragment().apply { arguments = args }
```

La fonction `apply`[^5] a pour signature :
```kotlin
inline fun <T> T.apply(block: T.() -> Unit): T
```

Finallement, on obtient le tableau suivant[^7] :

 | this | it
:- |:-: | -:
Retourne lui-même | `apply` | `also`
Retourne le résultat | `run` `with` | `let`

### {% icon fa-globe %} Références :

[^1]: [Documentation Kotlin officiel : with](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/with.html)
[^2]: [Documentation Kotlin officiel : run](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/run.html)
[^3]: [Documentation Kotlin officiel : let](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/let.html)
[^4]: [Documentation Kotlin officiel : also](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/also.html)
[^5]: [Documentation Kotlin officiel : apply](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/aply.html)
[^6]: [Ninja Functions in Kotlin. Understanding the power of generics](https://antonioleiva.com/generic-functions-kotlin/)
[^7]: [Kotlin — A deeper look](https://hackernoon.com/kotlin-a-deeper-look-8569d4da36f)
[^8]: [Mastering Kotlin standard functions: run, with, let, also and apply](https://medium.com/@elye.project/mastering-kotlin-standard-functions-run-with-let-also-and-apply-9cd334b0ef84)

*[AS]: Android Studio
[AK-2]: https://www.chillcoding.com/blog/2017/09/28/configurer-kotlin-projet-android/
[AK-4]: https://www.chillcoding.com/blog/2017/10/03/ajouter-extensions-kotlin/
[AK-4]:
