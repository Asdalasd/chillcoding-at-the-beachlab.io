---
title: "Comment monétiser une application mobile [AK 12]"
categories: fr coding tutoriel android kotlin
author: macha
---

<div class="text-center lead" markdown="1">
  ![Android Kotlin](/assets/img/post/android-kotlin.png)
</div>

Cet article présente les fonctions standard suivantes, inhérentes aux langage _Kotlin_ :

 + `with`
 + `run`
 + `let`
 + `also`
 + `apply`

Surnommées les «fonctions Ninjas» par Antonio Leiva[^6], ces fonctions font toutes
la même chose a quelques détails prêts.[^8] De manière général,
elles exécutent toutes un ensemble d'instructions sur une instance.
<!--more-->

## Application payante

Exemple : Application vendant les résultats d'un test ADN.

## Achat intégré

Exemple : Application de la catégorie jeu ou Application de contenu à lire (Journaux)

## Abonnement

## Publicité

CPC: rémunération au clique (ex : Adwords)
CPM: rémunération à l'affichage


"Native advertising": publicité placé dans le contenu de l'application plutôt qu'à des endroits classiques.

"Rewarded video ads": publicité vidéo proposée comme alternative à l'achat intégré.

L'annonceur le plus populaire est [Admob](https://admob.google.com/intl/fr_ALL/home/), de <i>Google</i>. Facile d'intégration, il permet de gagner de l'argent au CPC.

Annonceurs alternatifs :
Les annonceurs d'affiliation de contenu avec [NetBooster](https://www.netbooster.fr/) [Taboola](https://www.taboola.com/), [Outbrain](https://www.outbrain.com/),
ils choisissent aussi bien les annonceurs que les éditeurs d'application mobile.

L'annonceur d'affiliation, spécialisé dans l'application de la catégorie jeu [mobpartner](http://www6.mobpartner.com/)

L'annonceur spécialisé dans la vidéo, pour un modèle "Pay or see" [adcolony](https://www.adcolony.com/).

## Partenariat

Exemple : FootMercato et Betclic


### {% icon fa-globe %} Références :

1. [joptimisemonsite: Comment monetiser une application gratuite ?](https://www.joptimisemonsite.fr/monetiser-application-mobile-gratuite/)
2. [Free Apps For Me: 11 Best Apps that Pay You to Watch Ads](https://freeappsforme.com/apps-that-pay-you-to-watch-ads/)
3. [BusinessOfApps: Rewarded video ads networks](https://www.businessofapps.com/ads/rewarded-video/)
4. [Udonis: Top-earning ad networks](https://www.blog.udonis.co/mobile-marketing/mobile-apps/ecpms)
5. [Admixer: In-App ads during the crisis](https://blog.admixer.com/in-app-advertising-during-crisis/)
6. [Adjust: Best practice for rewarded video ads](https://www.adjust.com/blog/understanding-rewarded-video-ads/)
7. []()

*[AS]: Android Studio
[AK-2]: https://www.chillcoding.com/blog/2017/09/28/configurer-kotlin-projet-android/
[AK-4]: https://www.chillcoding.com/blog/2017/10/03/ajouter-extensions-kotlin/
[AK-4]:
