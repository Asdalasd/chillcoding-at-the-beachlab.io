# Formation : Android

**Baseline :** Apprenez à développer des applications Android au goût du jour rapidement !

**Durée :** 4 jours

## Public visé

Cette formation s’adresse aux développeurs ayant déjà une connaissance de la programmation orientée objet, plus spécifiquement Java. En particulier, elle s’adresse aux personnes souhaitant se lancer dans le développement d’applications mobiles sur Android.


## Description

Cette formation vous permettra d'acquérir les bases du **développement d'applications mobiles sur Android**. A l'issue des 4 jours, vous serez autonome dans la **réalisation d'applications Android**.
Tout au long de la formation, vous **mettrez en pratique la théorie** en créant une application Android vitrine présentant les principaux développements.

## Pré-requis

- Programmation orientée objet
- Ordinateur portable à apporter

## Objectifs pédagogiques

- Installer et configurer un environnement de développement sur sa machine
- Comprendre et écrire du code Android
- Construire une application Android en respectant les bonnes pratiques
- Publier son application sur le Play Store
- Connaître les principales libraries du monde Android

## Méthodes pédagogiques (administratif)
On visera une alternance de 50% de travaux pratiques et 50% de cours théoriques.
Les supports de cours seront fourni au format PDF accompagnés d’un lien vers le code source réalisé durant la formation.

## Evaluation des acquis pédagogiques (administratif)
Durant les 4 jours de formation, les TPs sont contrôlés et corrigés par le formateur

## Programme

#### Jour 1 : ABC d’Android et Interface Utilisateur Native

- **Présentation** et **historique** de la plateforme Android
- Installer et configurer l'**environnement de développement** sur sa machine
- Comprendre les **principes de programmation**
- Créer un premier projet 'Hello Android'
  - Arborescence du projet, fichiers clés
  - Familiarisation via la réalisation de tâches simples
  - Déploiement sur émulateur et appareil physique
- Connaître les principaux **éléments d'interface utilisateur native**
  - Vue
  - Ressource
  - Élément graphique natif
  - Message utilisateur
- Appréhender la philosophie du **Material Design**
- Créer une interface utilisateur élaborée
  - Construction d’une interface avec un agencement élaboré
  - Mise en pratique de quelques composants graphiques

#### Jour 2 : Outils du Développeur et Interface Utilisateur Interactive

- Connaître les principaux **outils du développeur**
  - Messages systèmes
  - Débugage
  - Déploiement
  - Tests
  - Librairies et références
- Anticiper la **compatibilité des versions** et le passage à Kotlin
- Importer des projets exemples
- Exploiter les messages d’erreur et le débugage
- Rendre son interface utilisateur interactive avec les **contrôleurs**
  - Adaptateur, utilisation avec une liste de données
  - Gestion des interactions
  - Navigation entre écrans
- Structurer son application
  - Utilisation des menus natifs
  - Utilisation des Fragments versus Activity
  - Communication d’informations entre Activity

#### Jour 3 : Gestion des données et communication réseau

- Stockage de données clé-valeur via les **préférences utilisateurs**
  - Conception des préférences
  - Interfaces de préférences
  - Fichiers de préférences
- Mettre en place des préférences dans son application
  - Utilisation des préférences de l’utilisateur (clé-valeur)
  - Création d’un écran de préférence
  - Récupération des valeurs depuis le fichier de préférences
- Envisager toutes les solutions de **persistance des données**
  - Système de fichiers
  - Base de données SQLite
- Enregistrer des données avec la librairie **Realm**
  - Stockage de données structurées
- Maîtriser les enjeux de la **communication réseau**
  - Contexte d’échange
  - Traitement en tâche de fond
  - Communication HTTP
- Communiquer avec un Web Service via la librairie **Retrofit**
  - Recevoir des données d’un Web Service
  - Envoyer des données à un Web Service

#### Jour 4 : Personnalisation d’interface utilisateur et publication

- Personnaliser l’interface utilisateur
  - **Animation**
  - Vue personnalisée
  - **Capteurs**
- Utiliser d’autres applications
- Créer une vue simple en code Java avec un **Canvas**
  - Animation graphiques et sonores d’éléments
  - Utilisation de l’accéléromètre pour animer un élément graphique
- Ouvrir l’application des paramètres
- **Prendre une photo** avec l’application native
- Préparer la **publication** de son application
  - App Store Optimization
  - Console de publication
  - Statistiques

  ### Pour aller plus loin

  #### Réussir une Publication sur le PlayStore
  Introduction à l’optimisation sur les boutiques en ligne d'application
  (<i>App Store Optimization</i>, <i>ASO</i>), la console de publication, et les statistiques.
  <ol type="A">
    <li>Publication</li>
    <li>Utilisateur</li>
    <li>Promotion</li>
  </ol>

  #### Multiplateforme avec Kotlin
  <ol type="A">
    <li>Application multiplateforme native</li>
    <li>Concept de bibliothèque <i>Kotlin</i>, pour <i>Android</i> et <i>iOS</i></li>
    <li>Architecture de l'environnement de développement</li>
  </ol>

  #### Pratique : Créer un premier projet multiplateforme
  * Configuration de l'environnement de développement
  * Création d’une première bibliothèque partagée
  <br>
  <br>

  #### Capteurs et Autres
  <ol type="A">
    <li>Accéléromètre</li>
    <li>Son</li>
    <li>Utiliser d’autres applications comme la Camera</li>
  </ol>

  #### Pratique : Créer un jeu
  * Animation graphiques et sonores d’éléments
  * Utilisation de l’accéléromètre pour animer un élément graphique
  * Prise d’une photo avec l’application native
  * Ouverture de l’application des paramètres
  <br>
  <br>

  #### Préférences d'un Utilisateur
  <ol type="A">
    <li>Conception des préférences avec <i>Material Design</i></li>
    <li>Interfaces de préférences</li>
    <li>Fichiers de préférences</li>
    <li>Enregistrement et lecture via une classe <i>Kotlin</i> déléguée</li>
  </ol>

  Support en ligne : [AK-7: SharedPreferences]

  #### Pratique : Enregistrer un profil utilisateur dans une Application
  * Utilisation des préférences de l’utilisateur (clé-valeur)
  * Création d’un écran de préférences
  * Récupération des valeurs depuis le fichier de préférences
  <br>
  <br>

  #### Géolocalisation et Cartographie
  <ol type="A">
    <li>Géolocalisation</li>
    <li>Carte géographique avec l'<i>API Google Maps</i></li>
  </ol>

  #### Pratique : Afficher la localisation de l’utilisateur
  * Utilisation des services _Google Maps_ dans une *Activity*
  *  Affichage de la dernière position détectée
  <br>
  <br>

  #### Montre Connectée avec Android Wear
  <ol type="A">
    <li>Interface et ses différents modes</li>
    <li>Configuration d’un projet <i>Android Wear</i></li>
    <li>Communication des données entre les modules</li>
  </ol>

  #### Pratique : Créer un premier projet sur Android Wear
  * Déploiement sur émulateur et sur montre
  <br>
  <br>

  #### Pratique : Approfondissement d'un développement classique
  * Ouverture d’un document PDF stocké en local
  * Enregistrement d’une image dans un fichier
  * Utilisation de *Fragment* avec *ViewPager* (menu à onglets)
  * Communication d’information entre *Fragment*
  <br>
  <br>

  #### La vérité sur l'affaire d'une application mobile
  <ol>
    <li>Chemin d'une application mobile</li>
    <li>Phase de découverte</li>
    <li>Phase de conception</li>
    <li>Phase de concrétisation</li>
    <li>Phase de lancément</li>
    <li>Phase de maintenance</li>
    <li>Application mobile en chemin</li>
  </ol>

  #### Pratique : Créer une application mobile de A à Z
  * Exploration du monde des applications
  * Fonctionnement de l'application dont vous avez eu l'idée
  * Choix technologique éclairé
  * Design de l'application (scénarios, tâches, arbres, maquettes)
  * Mise en place du projet et développement
  * Publications et tests utilisateurs
  * Promotion et soumissions alternatives
  * Maintenance
  <br>
  <br>


## La formatrice

![formation](https://gravatar.com/avatar/60d8d9b39be4c97c111c10867f24ca7f?s=100) Ingénieur en développement mobile depuis 2010, [Macha DA COSTA](https://twitter.com/MachaDaCosta) s’est spécialisée dans la conception, le développement et la publication d’applications mobiles Android. En tant qu'indépendante, elle enseigne à l’Université de Nice Sophia-Antipolis et dans des écoles privées. De plus, elle accompagne ses clients dans leurs projets mobiles ou web.
