---
title: "Bref, j'ai supprimé gitlab-ci.yml!"
categories: fr coding tutoriel android kotlin gitlab ci
author: macha
permalink: /android-gitlba-ci/
---






«
— Super : une mise à jour Android API 31, au top! Ça doit être stable après 3 mois...

/> _Exception in thread "main" java.lang.NoClassDefFoundError: javax/xml/bind/annotation/XmlSchema_

— ^^

/> _Caused by: java.lang.ClassNotFoundException: javax.xml.bind.annotation.XmlSchema_

— Ah mince, le CI est cassé !

/> _variables: JAVA_OPTS: "-XX:+IgnoreUnrecognizedVMOptions --add-modules java.xml.bind"_

— Why not!

/> _Unrecognized VM option 'IgnoreUnrecognizedVMOptions --add-modules java.xml.bind'_

— ^- Bref, j'ai supprimé gitlab-ci.yml!»

# Mettre en place un GitLab CI sur un projet Android [AK 12]

Cet article explique comment mettre en place un fichier d'intégration continue, _CI_ acronyme de
_Continuous Integration_, sur un projet _Android_ hébergé sur _GitLab_.

<!--more-->

En particulier, il est présenté un fichier `.gitlab-ci.yml`, appliqué à un projet _Android_ avec _Kotllin_.
Il convient pour l'_API Android 31_, la version de _JAVA 11 (JDK-11)_, et pour le
dernier outils en ligne de commande _Android_.

## Fichier .gitlab-ci.yml

À partir du fichier exemple fournit par _GitLab_[\[2\]](#gitlabciandroid),
il s'agit de vérifier les variables de versions :
 * `ANDROID_COMPILE_SDK`: correspond à `compileSdkVersion` de votre projet _Android_ (ici 31)
 * `ANDROID_BUILD_TOOLS`: correspond à `buildToolsVersion` (31.0.0)
 * `ANDROID_CMDLINE_TOOLS`: correspond à la dernière version du _command line tool Android_ [\[5\]](#cmdline) (8092744_latest)

Par ailleurs, _Android_ recommande l'utilisation de la variable `ANDROID_SDK_ROOT`
(`ANDROID_SDK_HOME` est déprécié).

Voici un exemple de fichier _.gitlab-ci.yml_ [\[2\]](#gitlabci) :
```yaml
image: openjdk:8-jdk

variables:
  ANDROID_COMPILE_SDK:    "30"
  ANDROID_BUILD_TOOLS:    "30.0.3"
  ANDROID_CMDLINE_TOOLS:  "6858069_latest"

before_script:
  - apt-get --quiet update --yes
  - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
  - wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_CMDLINE_TOOLS}.zip
  - mkdir android-sdk-linux
  - unzip -d android-sdk-linux/cmdline-tools android-sdk.zip
  - mv android-sdk-linux/cmdline-tools/cmdline-tools android-sdk-linux/cmdline-tools/latest
  - echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
  - echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "platform-tools" >/dev/null
  - echo y | android-sdk-linux/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
  - export ANDROID_SDK_ROOT=$PWD/android-sdk-linux
  - export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools/
  - chmod +x ./gradlew
  # temporarily disable checking for EPIPE error and use yes to accept all licenses
  - set +o pipefail
  - yes | android-sdk-linux/tools/bin/sdkmanager --licenses
  - set -o pipefail

stages:
  - build

lintDebug:
  stage: build
  script:
    - ./gradlew -Pci --console=plain :app:lintDebug -PbuildDir=lint

assembleDebug:
  stage: build
  script:
    - ./gradlew assembleDebug
  artifacts:
    paths:
    - app/build/outputs/apk/debug/app-debug.apk

```

Après mise à jour :
```yaml
image: openjdk:11-jdk

variables:
  ANDROID_COMPILE_SDK:    "31"
  ANDROID_BUILD_TOOLS:    "31.0.0"
  ANDROID_CMDLINE_TOOLS:  "8092744_latest"
```
Ça ne fonctionne plus, quelques pistes :
 * Ajout de la variable _JAVA_OPTS: "-XX:+IgnoreUnrecognizedVMOptions --add-modules java.xml.bind"_  
 ou _JAVA_OPTS: "-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee"_
 * Ajout des dépendances [\[9\]](#jabx)
 * Utilisation d'un fichier [docker](https://github.com/saschpe/docker-android-sdk/blob/master/Dockerfile#L45) ?

Au final, l'image _Docker_ d'_inovex_ [\[10\]](#inovex) fonctionne très bien :
```yaml
image: inovex/gitlab-ci-android

stages:
- release

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

before_script:
- export GRADLE_USER_HOME=$(pwd)/.gradle
- chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
  - .gradle/

build:
    stage: release
    script:
        - ./gradlew clean assembleRelease
    artifacts:
        expire_in: 2 weeks
        paths:
            - app/build/outputs/apk/*.apk
    only:
        - develop
```

## Variables de clé secrète

1. Allez dans _Settings > CI/CD > Variables_, puis _Expand_
2. Ajoutez une variable _KEYS_XML_ avec pour valeurs vos clés secrètes, voici un exemple :

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="base64_encoded_public_key" translatable="false">trucbidule</string>
</resources>
```
3. Ajoutez les clés dans le fichier _XML Android_ depuis _.gitlab-ci.yml_, dans la partie `before_script` :

```
- echo $KEYS_XML > ./app/src/main/res/values/keys.xml
```

{% include aside.html %}

Finalement, cet article dévoile un fichier `gitlab-ci.yml`; pour les dernières versions d'_Android_, géré par une image _docker_.

### {% icon fa-globe %} Références :

1. <a name="gitlabci"></a>[The ideotec blog: Android GitLab CI Pipeline in 2020](https://blog.ideotec.es/android-gitlab-ci-pipeline-2020/)
2. <a name="gitlabciandroid"></a>[GitLab: Setting up GitLab CI for Android projects](https://about.gitlab.com/blog/2018/10/24/setting-up-gitlab-ci-for-android-projects/)
3. [GitLab: How to publish Android apps to the Google Play Store with GitLab and fastlane](https://about.gitlab.com/blog/2019/01/28/android-publishing-with-gitlab-and-fastlane/)
4. <a name=""></a>[GitLab: Gitlab CI CD documentation](https://docs.gitlab.com/ee/ci/)
5. <a name="cmdline"></a>[developer.android: Android Studio command line tools](https://developer.android.com/studio/index.html)
6. [developer.android: Environment variables](https://developer.android.com/studio/command-line/variables#android_sdk_root)
7. [Google Issue Tracker: About jdk 11 and sdk manager](https://issuetracker.google.com/issues/67495440)
8. <a name="travis"></a>[stackoverflow: Travis CI example](https://stackoverflow.com/questions/53076422/getting-android-sdkmanager-to-run-with-java-11)
9. <a name="jabx"></a> [stackoverflow: JABX Dependencies](https://stackoverflow.com/questions/43574426/how-to-resolve-java-lang-noclassdeffounderror-javax-xml-bind-jaxbexception/43574427#43574427)
10. <a name="inovex"></a> [Inovex: docker img](https://github.com/inovex/gitlab-ci-android)
