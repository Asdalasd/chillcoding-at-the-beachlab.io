package com.chillcoding.magic

data class MagicCircle(val maxX: Int, val maxY: Int) {

    var cx = (0..maxX).random().toFloat()
    var cy = (0..maxY).random().toFloat()
    val rad: Float = 40F
    var dx = DELTA
    var dy = DELTA

    val mColor = App.sColors[App.sColors.indices.random()]

    companion object {
        const val DELTA = 9
    }

    fun move() {
        when {
            cx.toInt() !in 0..maxX -> dx = -dx
            cy.toInt() !in 0..maxY -> dy = -dy
        }
        cx += dx
        cy += dy
    }

    fun moveTo(x: Float, y: Float) {
        cx = x
        cy = y
    }

    fun move(dx: Float, dy: Float) {
        when {
            cx.toInt() !in 0..maxX -> {
                if (dx < 0)
                    cx = 0F
                else
                    cx = maxX.toFloat()
            }
            cy.toInt() !in 0..maxY -> {
                if (dy > 0)
                    cy = 0F
                else
                    cy = maxY.toFloat()
            }
        }
        cx -= dx * DELTA
        cy += dy * DELTA
    }
}

