package com.chill.beach

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chill.beach.databinding.ItemBeachBinding

class BeachAdapter(val items: List<Beach>) :
    RecyclerView.Adapter<BeachAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBeachBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindBeach(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: ItemBeachBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindBeach(beach: Beach) {
            binding.beachName.text = beach.name
            binding.beachImg.setImageResource(beach.img)
        }
    }
}
