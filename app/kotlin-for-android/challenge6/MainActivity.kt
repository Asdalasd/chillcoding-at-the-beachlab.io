package com.chill.beach

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.chill.beach.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    var items = mutableListOf<Beach>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        seedItems()
        binding.beachRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.beachRecyclerView.adapter = BeachAdapter(items)
    }

    private fun seedItems() {
        val beachString = resources.getStringArray(R.array.beach)
        val imgArray = intArrayOf(
            R.drawable.ic_sun,
            R.drawable.ic_love,
            R.drawable.ic_happy,
            R.drawable.ic_circle,
            R.drawable.ic_wind
        )
        for (k in 0..beachString.size - 1) {
            items.add(Beach(beachString[k], imgArray[k]))
        }
    }
}
