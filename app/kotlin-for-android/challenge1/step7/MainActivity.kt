package com.chill.first

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button = findViewById<Button>(R.id.mainButton)
        button.setOnClickListener {
            Toast.makeText(this, R.string.title_hello, Toast.LENGTH_LONG).show()
        }
    }
}