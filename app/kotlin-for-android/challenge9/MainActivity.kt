package com.chill.network

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.chill.network.`interface`.CountriesService
import com.chill.network.model.Country
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MainActivity : AppCompatActivity() {

    companion object {
        const val URL_COUNTRY_API = "https://restcountries.eu/"
    }

    lateinit var countryRequest: Call<List<Country>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareRequests()
        makeCountriesRequest()
    }

    private fun prepareRequests() {
        val retro = Retrofit.Builder()
            .baseUrl(URL_COUNTRY_API)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        val service = retro.create(CountriesService::class.java)
        countryRequest = service.listCountries()
    }

    private fun makeCountriesRequest() {
        countryRequest.enqueue(object : Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                val allCountry = response.body()
                for (c in allCountry!!)
                    Log.v(
                        MainActivity::class.simpleName,
                        "NAME: ${c.name} \n CAPITAL: ${c.capital} \n Language: ${c.languages} "
                    )
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                Log.i(MainActivity::class.simpleName, "on FAILURE!!!!")
            }

        })
    }
}